﻿namespace TabCorp.ToteBetting.Domain
{
    public class PlaceBetDividend
    {
        public decimal FirstPlaceHorseYield { get; set; }
        public decimal SecondPlaceHorseYield { get; set; }
        public decimal ThirdPlaceHorseYield { get; set; }
    }
}