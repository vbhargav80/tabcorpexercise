﻿namespace TabCorp.ToteBetting.Domain
{
    public enum ProductType
    {
        Win,
        Place,
        Exacta
    }
}