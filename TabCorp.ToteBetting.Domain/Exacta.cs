﻿using System;

namespace TabCorp.ToteBetting.Domain
{
    public class Exacta : Bet
    {
        public Exacta(string input) : base(input)
        {
            var fragments = input.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
            var placesFragments = fragments[2].Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
            if (placesFragments.Length != 2)
            {
                throw new ArgumentException("Invalid input for exacta: " + input);
            }

            FirstPlaceHorseNumber = int.Parse(placesFragments[0]);
            SecondPlaceHorseNumber = int.Parse(placesFragments[1]);
            Stake = decimal.Parse(fragments[3]);
        }

        public int FirstPlaceHorseNumber { get; private set; }
        public int SecondPlaceHorseNumber { get; private set; }
    }
}