﻿using System;
using System.Security.Policy;

namespace TabCorp.ToteBetting.Domain
{
    public class Result
    {
        public int FirstPlaceHorseNumber { get; private set; }
        public int SecondPlaceHorseNumber { get; private set; }
        public int ThirdPlaceHorseNumber { get; private set; }

        public Result(string input)
        {
            var fragments = input.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (fragments.Length != 4)
            {
                throw new ArgumentException("Invalid input for result: " + input);
            }

            FirstPlaceHorseNumber = int.Parse(fragments[1]);
            SecondPlaceHorseNumber = int.Parse(fragments[2]);
            ThirdPlaceHorseNumber = int.Parse(fragments[3]);
        }
    }
}