﻿using System;

namespace TabCorp.ToteBetting.Domain
{
    public class Win : Bet
    {
        public int WinningHorseNumber { get; private set; }

        public Win(string input) : base(input)
        {
            var fragments = input.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            WinningHorseNumber = int.Parse(fragments[2]);
            Stake = decimal.Parse(fragments[3]);
        }
    }
}