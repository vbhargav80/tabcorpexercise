﻿using System;

namespace TabCorp.ToteBetting.Domain
{
    public class Place : Bet
    {
        public int WinningHorseNumber { get; private set; }

        public Place(string input) : base(input)
        {
            var fragments = input.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            WinningHorseNumber = int.Parse(fragments[2]);
            Stake = decimal.Parse(fragments[3]);
        }
    }
}