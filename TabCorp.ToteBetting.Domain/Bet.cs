﻿namespace TabCorp.ToteBetting.Domain
{
    public class Bet
    {
        protected readonly string _input;
        public decimal Stake { get; set; }

        public Bet(string input)
        {
            _input = input;
        }
    }
}