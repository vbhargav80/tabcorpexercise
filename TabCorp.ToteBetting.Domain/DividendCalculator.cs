﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TabCorp.ToteBetting.Domain
{
    public class DividendCalculator
    {
        private readonly Result _result;
        private readonly List<Win> _winBets;
        private readonly List<Place> _placeBets;
        private readonly List<Exacta> _exactaBets;

        public DividendCalculator(List<Win> winBets, List<Place> placeBets, List<Exacta> exactaBets, Result result)
        {
            _winBets = winBets;
            _placeBets = placeBets;
            _exactaBets = exactaBets;
            _result = result;
        }

        public decimal GetWinningBetDividend()
        {
            var winningHorse = _result.FirstPlaceHorseNumber;
            var winningPunters = _winBets.Where(a => a.WinningHorseNumber == winningHorse);

            var totalStake = _winBets.Sum(a => a.Stake);
            var totalStakeOfWinners = winningPunters.Sum(a => a.Stake);

            var tabcorpCommission = totalStake*(decimal) 0.15;
            var distributable = totalStake - tabcorpCommission;

            var dividend = Math.Round(distributable/totalStakeOfWinners, 2);
            return dividend;
        }

        public PlaceBetDividend GetPlaceBetDividend()
        {
            var firstPlaceWinners = _placeBets.Where(a => a.WinningHorseNumber == _result.FirstPlaceHorseNumber);
            var secondPlaceWinners = _placeBets.Where(a => a.WinningHorseNumber == _result.SecondPlaceHorseNumber);
            var thirdPlaceWinners = _placeBets.Where(a => a.WinningHorseNumber == _result.ThirdPlaceHorseNumber);

            var totalStake = _placeBets.Sum(a => a.Stake);
            var totalStakeOfFirstPlaceWinners = firstPlaceWinners.Sum(a => a.Stake);
            var totalStakeOfSecondPlaceWinners = secondPlaceWinners.Sum(a => a.Stake);
            var totalStakeOfThirdPlaceWinners = thirdPlaceWinners.Sum(a => a.Stake);

            var tabcorpCommission = totalStake * (decimal)0.12;
            var distributable = totalStake - tabcorpCommission;

            var thirdOfDistributable = distributable/3;

            var firstPlaceDividend = Math.Round(thirdOfDistributable / totalStakeOfFirstPlaceWinners, 2);
            var secondPlaceDividend = Math.Round(thirdOfDistributable / totalStakeOfSecondPlaceWinners, 2);
            var thirdPlaceDividend = Math.Round(thirdOfDistributable / totalStakeOfThirdPlaceWinners, 2);

            var result = new PlaceBetDividend()
            {
                FirstPlaceHorseYield = firstPlaceDividend,
                SecondPlaceHorseYield = secondPlaceDividend,
                ThirdPlaceHorseYield = thirdPlaceDividend
            };

            return result;
        }

        public decimal GetExactaBetDividend()
        {
            var firstPlaceHorseNumber = _result.FirstPlaceHorseNumber;
            var secondPlaceHorseNumber = _result.SecondPlaceHorseNumber;

            var winningPunters = _exactaBets.Where(
                a => a.FirstPlaceHorseNumber == firstPlaceHorseNumber &&
                a.SecondPlaceHorseNumber == secondPlaceHorseNumber
                );

            var totalStake = _exactaBets.Sum(a => a.Stake);
            var totalStakeOfWinners = winningPunters.Sum(a => a.Stake);

            var tabcorpCommission = totalStake * (decimal)0.18;
            var distributable = totalStake - tabcorpCommission;

            var dividend = Math.Round(distributable / totalStakeOfWinners, 2);
            return dividend;
        }
    }
}
