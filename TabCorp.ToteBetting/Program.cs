﻿using System;
using TabCorp.ToteBetting.Domain;

namespace TabCorp.ToteBetting
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Please enter valid input. Result must be on the last line");
                var inputParser = new InputParser();
                inputParser.Parse();

                var dividendCalculator = new DividendCalculator(
                    inputParser.WinBets,
                    inputParser.PlaceBets,
                    inputParser.ExactaBets,
                    inputParser.Result
                    );

                Console.WriteLine("\n\nDividends");

                var winningBetDividend = dividendCalculator.GetWinningBetDividend();
                Console.WriteLine("{0}:{1}:{2}", "Win", inputParser.Result.FirstPlaceHorseNumber, winningBetDividend.ToString("C"));

                var placeBetDividend = dividendCalculator.GetPlaceBetDividend();
                Console.WriteLine("{0}:{1}:{2}", "Place", inputParser.Result.FirstPlaceHorseNumber, placeBetDividend.FirstPlaceHorseYield.ToString("C"));
                Console.WriteLine("{0}:{1}:{2}", "Place", inputParser.Result.SecondPlaceHorseNumber, placeBetDividend.SecondPlaceHorseYield.ToString("C"));
                Console.WriteLine("{0}:{1}:{2}", "Place", inputParser.Result.ThirdPlaceHorseNumber, placeBetDividend.ThirdPlaceHorseYield.ToString("C"));

                var exactaBetDividend = dividendCalculator.GetExactaBetDividend();
                Console.WriteLine(
                    "{0}:{1},{2}:{3}",
                    "Exacta",
                    inputParser.Result.FirstPlaceHorseNumber,
                    inputParser.Result.SecondPlaceHorseNumber,
                    exactaBetDividend.ToString("C"));
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an error");
                Console.WriteLine(ex.Message);

                while (Console.KeyAvailable)
                {
                    Console.ReadKey(false);
                }
            }
            
            Console.WriteLine("\n\nPlease enter any key to end");
            Console.Read();
        }
    }
}