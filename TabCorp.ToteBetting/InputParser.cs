﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using TabCorp.ToteBetting.Domain;

namespace TabCorp.ToteBetting
{
    public class InputParser
    {
        private int _numberofLines;
        private const int MaxLines = 1000;

        public InputParser()
        {
            WinBets = new List<Win>();
            PlaceBets = new List<Place>();
            ExactaBets = new List<Exacta>();
        }

        public List<Win> WinBets { get; }
        public List<Place> PlaceBets { get; }
        public List<Exacta> ExactaBets { get; }
        public Result Result { get; private set; }

        public void Parse()
        {
            string input = string.Empty;
            while ((input = Console.ReadLine()) != null)
            {
                _numberofLines++;

                var tokens = input.Split(new char[] {':'}, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 4)
                {
                    continue;
                }

                var inputType = tokens[0].ToUpper();
                if (!(inputType == "BET" || inputType == "RESULT"))
                {
                    continue;
                }

                var productType = tokens[1].ToUpper();
                if (productType == "W")
                {
                    WinBets.Add(new Win(input));
                }
                else if (productType == "P")
                {
                    PlaceBets.Add(new Place(input));
                }
                else if (productType == "E")
                {
                    ExactaBets.Add(new Exacta(input));
                }
                else if (inputType == "RESULT")
                {
                    Result = new Result(input);
                }

                if (input.ToUpper().StartsWith("RESULT"))
                {
                    break;
                }

                if (_numberofLines > MaxLines)
                {
                    break;
                }
            }
        }
    }
}